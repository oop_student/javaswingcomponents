/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontapat.javaswingcomponent;

import javax.swing.*;

/**
 *
 * @author DELL
 */
public class JSliderEx extends JFrame {

    public JSliderEx() {
        JSlider slider = new JSlider(JSlider.VERTICAL, 0, 50, 25);
        JPanel panel = new JPanel();
        panel.add(slider);
        add(panel);
    }

    public static void main(String s[]) {
        JSliderEx frame = new JSliderEx();
        frame.pack();
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
