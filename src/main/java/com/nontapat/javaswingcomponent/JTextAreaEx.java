/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontapat.javaswingcomponent;

import javax.swing.*;

/**
 *
 * @author DELL
 */
public class JTextAreaEx {

    public JTextAreaEx() {
        JFrame f= new JFrame();  
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JTextArea area=new JTextArea("Welcome to javatpoint");  
        area.setBounds(10,30, 200,200);  
        f.add(area);  
        f.setSize(300,300);  
        f.setLayout(null);  
        f.setVisible(true);  
    }
    public static void main(String[] args) {
        new JTextAreaEx();
    }
    
     
}
