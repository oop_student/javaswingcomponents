/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontapat.javaswingcomponent;

import javax.swing.*;

/**
 *
 * @author DELL
 */
public class JScrollBarEx {

    JScrollBarEx() {
        JFrame f = new JFrame("Scrollbar Example");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JScrollBar s = new JScrollBar();
        s.setBounds(100, 100, 15, 100);
        f.add(s);
        f.setSize(400, 400);
        f.setLayout(null);
        f.setVisible(true);
    }

    public static void main(String args[]) {
        new JScrollBarEx();
    }
}

