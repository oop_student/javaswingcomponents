/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontapat.javaswingcomponent;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * @author DELL
 */
public class JTreeEx {

    JFrame f;

    JTreeEx() {
        f = new JFrame();
        DefaultMutableTreeNode style = new DefaultMutableTreeNode("Style");
        DefaultMutableTreeNode color = new DefaultMutableTreeNode("color");
        DefaultMutableTreeNode font = new DefaultMutableTreeNode("font");
        style.add(color);
        style.add(font);
        DefaultMutableTreeNode red = new DefaultMutableTreeNode("red");
        DefaultMutableTreeNode blue = new DefaultMutableTreeNode("blue");
        DefaultMutableTreeNode black = new DefaultMutableTreeNode("black");
        DefaultMutableTreeNode green = new DefaultMutableTreeNode("green");
        color.add(red);
        color.add(blue);
        color.add(black);
        color.add(green);
        DefaultMutableTreeNode tahoma = new DefaultMutableTreeNode("tahoma");
        DefaultMutableTreeNode timeroman = new DefaultMutableTreeNode("timeroman");
        DefaultMutableTreeNode sarabun = new DefaultMutableTreeNode("sarabun");
        font.add(tahoma);
        font.add(timeroman);
        font.add(sarabun);
        JTree jt = new JTree(style);
        f.add(jt);
        f.setSize(200, 200);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }

    public static void main(String[] args) {
        new JTreeEx();
    }
}
